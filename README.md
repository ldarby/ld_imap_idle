## ld\_imap_idle.pl

This perl program uses IMAP IDLE to notify about new emails and has the following features:

* Only notifies about desired emails, not all emails.
* Rate limits the notifications if they're too frequent (without rate limiting high priority emails).
* Can use Pidgin's purple-remote command, to avoid notifying if you're not 'available' (so notifications are automatically disabled when you're away and during meetings/presentations).

Requires these CPAN modules: Mail::IMAPClient, Desktop::Notify, Term::ReadKey and Proc::PID::File.

To get started, you'll need to tell it what emails you want to be notified for, by creating a rules file in the following format: a header name followed by a comma separated list of regexes, and each regex followed by '=number'.  Note that regex chars need to be escaped.  Example:

    To  my@email\.com=14, mailing@list\.com=12
    CC  my@email\.com=12, mailing@list\.com=12
    From your@boss\.com=12
    Subject spam=-20,
    Importance High=12

Incoming emails are scored by adding up the scores of the regexes matched then the following rule is used:
 
    score < 10: don't notify
    score >= 10 and score < 13: queue the notification, then send it after a timeout since last notification.
    score >= 13: notify immediately (including any pending notifications)

The timeout for the delay is set with the `--min_period` setting.

Usage:
For all options:

    ld_imap_idle.pl --help

Minimum options needed:

    ld_imap_idle.pl --username <your user name> --server <your imap server> --rules <your rules file> 

TODO:
* Expressions in rules, like From == .. AND Subject == ...
* Body filtering (must have expressions first)

Other ideas for future development:

* More notification types, e.g. sound, SMS.
* Different alerts for different scores.
* Drop the scoring mechanism completely and use a Bayesian spam filter like method instead. I.e. instead of learning what emails are spam, it learns what you want to be notified about.
