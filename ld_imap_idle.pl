#!/bin/sh
eval 'exec perl -T -x -S $0 ${1+"$@"}'
{};
#!perl
#line 6
#The above execs perl in any location in $PATH with -T.
#Using -T and other checks are necessary because data from the network
#(emails) gets passed to /bin/sh or eval.

# Copyright 2016,2018 Laurence Darby

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use warnings;
use strict;
use Carp;
use Time::HiRes qw(usleep clock_gettime CLOCK_MONOTONIC);
use POSIX qw(strftime);
use Data::Dumper;
use Getopt::Long;
use Encode;

#cpan
use Mail::IMAPClient;
use Desktop::Notify;
use Term::ReadKey;
use IO::Socket::SSL;
use Proc::PID::File;

#in case of hangs uncomment this, repro the hang, and send WINCH signal
# use Devel::StackTrace;
# sub bt
# {
#	 my $trace = Devel::StackTrace->new;
#	 print STDERR $trace->as_string;
# }
#$SIG{WINCH} = \&bt;

$SIG{__WARN__} = \&confess;
$SIG{INT} = \&cleanup;
$SIG{TERM} = \&cleanup;

$| = 1;

#user specified options:
my $server = "";
my $port = 0;
my $username = "";
my $password_file = "";
my $usessl = 1;
my $usestarttls = 0;
my $rules = "";
my $help = 0;
my $max_idle_time = 29;
my $min_period = 0;
my $debug = 1;
my $use_libpurple_status = 0;
my $password = "";
my $mua = "";

#save a copy for printing in help
my $default_usessl = $usessl;
my $default_usestarttls = $usestarttls;
my $default_max_idle_time = $max_idle_time;
my $default_min_period = $min_period;
my $default_debug = $debug;
my $default_use_libpurple_status = $use_libpurple_status;

#could be configurable
my $min_alert_score = 10;
my $delayed_alert_score = 13;

#other globals
my $timeout = 5;
my $now_bucket = 0;
my $old_now_bucket = 0;
my $time_since_last_alert = 0;
my $last_alert_time = 0;
my $max_score = 0;
my $now;
my %idle_rate;
my %msg_buffer;
my $notify;
my %notification_rules;
my $client;
my $idle;
my $idling;
my $idle_start;
my $idle_end;
my %processed;
my @fetch = ();

sub dprint;
sub t_fmt;

my ($imapclient_version_major,$imapclient_version_minor) = split(/\./,$Mail::IMAPClient::VERSION);
if ($imapclient_version_major < 3 ||
    ($imapclient_version_major == 3 && $imapclient_version_minor < 39)) {
	#https://rt.cpan.org/Public/Bug/Display.html?id=97718
	dprint 0, "Warning: Mail::IMAPClient version $Mail::IMAPClient::VERSION is old, please upgrade it.\n";
}

my $pid = $$;
$pid = untaint($pid);
$ENV{PATH} = untaint($ENV{PATH}); #doesn't cause taint mode to ignore insecure directories

#Warn about insecure $PATH now rather than later.
#If this dies here it's likely you have . in your $PATH which is insecure.
system("true");

my $xmessage = `which xmessage 2> /dev/null`;
chomp($xmessage);
if (! -x $xmessage) {
	dprint "Warning: xmessage not found, unable to fallback to it in case Desktop::Notify fails.  Install xmessage to resolve this.\n";
}

$SIG{__WARN__} = undef;
GetOptions(
	"server=s"             => \$server,
	"port=i"               => \$port,
	"username=s"           => \$username,
	"usessl=i"             => \$usessl,
	"usestarttls=i"        => \$usestarttls,
	"rules=s"              => \$rules,
	"min_period=i"         => \$min_period,
	"help"                 => \$help,
	"max_idle_time=i"      => \$max_idle_time,
	"use_libpurple_status" => \$use_libpurple_status,
	"debug=i"              => \$debug,
	"password_file=s"      => \$password_file,
	"mua=s"                => \$mua,
    ) || ($help = 1);
$SIG{__WARN__} = \&confess;

my $help2 = $help;
if ($rules eq "" && !$help2) {
	print "\n--rules file must be specified\n\n";
	$help = 1;
}

if ($username eq "" && !$help2) {
	print "\n--username must be specified\n\n";
	$help = 1;
}

if ($server eq "" && !$help2) {
	print "\n--server must be specified\n\n";
	$help = 1;
}

if ($help) {
	print "\nUsage: $0 options\n";
	print "\nRequired options are: \n";
	print " --username [username]\n";
	print " --server [server]\n";
	print " --rules [file]\n";
	print "\nOptional options are: \n";
	print " --port [port] (default: 993 for ssl, 143 for plain && starttls)\n";
	print " --usessl=[1|0] (default: $default_usessl)\n";
	print " --usestarttls=[1|0] (default: $default_usestarttls)\n";
	print " --max_idle_time [mins] - should be set appropriately for the server (default: $default_max_idle_time)\n";
	print " --min_period [mins] - minimum delay between alerts - if emails come in faster, alerts will be batched (default: $default_min_period)\n";
	print " --use_libpurple_status - don't alert if libpurple's status is e.g. busy or away (default: $default_use_libpurple_status)\n";
	print " --password_file - read password from a file, must be in format: password=<password>\n";
	print " --mua - the window title of your mail client. Set this to *prevent* alerting, if this window is active\n";
	print "         (i.e. if you are currently reading emails, do you really want alerts about ones you haven't read yet?)\n";
	print " --debug [0|1|2|3] verbosity (default $default_debug) (NOTE: 3 prints all IMAP traffic, which may include passwords)\n";
	print "\n";
	print "\n";
	exit();
}

$server = untaint($server);
$port = untaint($port);
$rules = untaint($rules);
$usessl = untaint($usessl);
$usestarttls = untaint($usestarttls);
$max_idle_time = untaint($max_idle_time);
$min_period = untaint($min_period);
$debug = untaint($debug);
$password_file = untaint($password_file);
$mua = untaint($mua);

#convert to seconds
$max_idle_time *= 60;
$min_period *= 60;

%notification_rules = get_rules($rules);

if ($use_libpurple_status) {
	my $purple = `which purple-remote`;
	chomp($purple);
	if (! -x $purple) {
		dprint "Error: --use_libpurple_status specified, but purple-remote command can't be used. Install pidgin for this.\n";
		exit(1);
	}
}

if (!-e $password_file) {
	print "Enter password: ";
	ReadMode('noecho'); # don't echo
	chomp($password = <STDIN>);
	ReadMode(0);	# back to normal
	print "\n";
} else {
	open(F, "< $password_file") || die("couldn't open password_file $password_file: $!\n");
	while (<F>) {
		my $line = $_;
		if ($line =~ /password=(.*)$/) {
			$password = $1;
			last;
		}
	}
	close(F);
	if ($password eq "") {
		die "couldn't get password from file $password_file\n";
	}
}

#allow mulitple copies to run, but not against the same server
use Proc::Pidfile;
my $pidfile = Proc::Pidfile->new(pidfile => "/tmp/imap-idle-$server.pid");

imap_connect();
my $features = "@{$client->capability}" || die "Could not determine capability: ", $client->LastError;

dprint 2, "features = $features\n";

if ($features !~ /IDLE/) {
	die "IDLE not found in features string: $features\n";
}

$/ = "\r\n";

#Main loop, should never exit, apart from by ctrl-c or kill signal.
#Or password change, but it should notfy you in that case.
while (1) {
	$now = clock_gettime(CLOCK_MONOTONIC);
	dprint 2, "main, number of pending notifications = " . scalar(keys %msg_buffer) . "\n";
	if (scalar(keys %msg_buffer) > 0) {
		send_alerts($now);
	}
	if (!$idling) {
		re_idle();
	} elsif ((($now - $idle_start) > $max_idle_time)) {
		dprint 2, "will hit max_idle_time after max_idle_time $max_idle_time seconds, re_idleing\n";
		re_idle();
	}
	my $idlemsgs;
	my $socket_wait_time = $max_idle_time - ($now - $idle_start);
	#If there are pending batch alerts, possibly shorten socket read time, so we wake up and alert on time.
	my $next_alert_time = $last_alert_time + $min_period;
	my $next_wake_up = $idle_start + $max_idle_time;
	if (scalar(keys %msg_buffer) > 0 &&
	    $next_alert_time > $now &&
	    $next_alert_time < $next_wake_up - 5) {
		dprint 2, "next_alert_time = $next_alert_time\n";
		dprint 2, "next_wake_up = $next_wake_up\n";
		dprint 2, "now = $now\n";
		$socket_wait_time = $next_alert_time - $now + 4;
	}
	$socket_wait_time = int($socket_wait_time);
	if ($socket_wait_time < 1) {
		dprint "ERROR: idle_time = $socket_wait_time, resetting to $max_idle_time\n";
		$socket_wait_time = $max_idle_time;
	}
	dprint 2, "calling idle_data($socket_wait_time)\n";
	$idlemsgs = $client->idle_data($socket_wait_time);
	dprint 2, "idle_data returned\n";
	if (!defined($idlemsgs)) {
		dprint "idle_data: LastError: " . $client->LastError . "\n";
		dprint "Reconnecting in 5 seconds\n";
		sleep(5);
		imap_connect();
		next;
	}
	if ("@{$idlemsgs}" eq "") {
		my $after = clock_gettime(CLOCK_MONOTONIC);
		if (abs(($after - $now) - $socket_wait_time) < 1.5) {
			dprint 2, "got no data, timeout\n";
			if (($after - $idle_start) > ($max_idle_time - 2)) {
				done_if_idle();
			}
		} else {
			#Suspend & resume can trigger this.
			dprint "Warning: sleep time " . ($after - $now) . " seconds different from socket_wait_time: $socket_wait_time\n";
		}
		next;
	}
	dprint 2, "got idle_data:\n";
	foreach my $resp (@{$idlemsgs}) {
		chomp($resp);
		dprint 2, " =>$resp<=\n";
		if ($resp =~ /^\* (\d+) FETCH/) {
			#* 6134 FETCH (FLAGS (\Seen))
			#Lots of these can happen if another mail client marks a bunch of mails as read, so buffer these up and process all at once.
			push @fetch, $resp;
		} elsif ($resp =~ /^\* (\d+) RECENT/) {
			my $recent_count = $1;
			dprint 2, "$recent_count RECENT\n";
			done_if_idle();
			my $output = $client->recent() || dprint 2, "No recent msgs: $@\n";
			dprint 2, scalar(@{$output}) . " messsages from recent()\n";
			if (scalar(@{$output}) == 0 && $recent_count > 0) {
				dprint "Warning: no output from recent(), but $recent_count recent messages exist\n";
				next;
			}
			my $hashref = $client->parse_headers($output, "Date", "Subject", "From", "To", "CC", "Importance") || dprint "Warning: Could not parse_headers: $@\n";
			if (ref ($hashref) ne "HASH") {
				$hashref = {};
			}
			dprint 3, Dumper($hashref);
			foreach my $msg_uid (keys %{$hashref}) {
				my $bad = "";
				dprint 2, "msg_uid = $msg_uid\n";
				foreach ("Date", "Subject", "From", "To") {
					if (!(defined($hashref->{$msg_uid}->{$_}))) {
						$bad = "$_";
					}
				}
				if ($bad ne "") {
					dprint "parse_headers returned unprocessable $msg_uid, missing $bad:\n";
					dprint Dumper($hashref->{$msg_uid});
					$processed{$msg_uid} = 1; #mark as processed just so this isn't repeatedly logged.
				} elsif (!defined($processed{$msg_uid})) {
					process_msg($hashref->{$msg_uid}, $msg_uid);
					$processed{$msg_uid} = 1;
				}
			}
		} elsif ($resp =~ /^\* BYE/) {
			sleep(5);
			imap_connect();
			next;
		}
	}
	if (scalar(@fetch) > 0) {
		foreach my $resp (@fetch) {
			$resp =~ /^\* (\d+) FETCH/;
			my $msg_seq_no = $1;
			if ($resp =~ /\\Seen/ && $resp !~ /\\Deleted/) {
				#There's probably a better way of doing this.
				#Don't alert for emails that get read before alerting, which could happen if alerts were batched.
				#But if it's been deleted, we can't get the message id to avoid alerting for it. :(
				done_if_idle();
				my $msg_uid = ${$client->search($client->Quote($msg_seq_no))}[0];
				if (!defined($msg_uid)) {
					dprint "couldn't get msg_uid from seq_no $msg_seq_no, was it deleted?\n";
				} elsif (defined($msg_buffer{$msg_uid})) {
					dprint 2, "deleting msg_seq_no $msg_seq_no, msg_uid $msg_uid from msg_buffer\n";
					delete($msg_buffer{$msg_uid});
				}
				dprint 2, "resp = $resp, number of pending notifications = " . scalar(keys %msg_buffer) . "\n";
			}
		}
		@fetch = ();
	}
}



sub re_idle
{
	done_if_idle();
	dprint 2, "Idling:\n";
	$now = clock_gettime(CLOCK_MONOTONIC);
	$old_now_bucket = $now_bucket;
	$now_bucket = sprintf("%0.0f", $now / 60) + 0;
	if ($old_now_bucket != $now_bucket) {
		delete $idle_rate{$old_now_bucket};
	}
	if (++$idle_rate{$now_bucket} > 15) {
		#fail safe check in case of bugs elsewhere that cause a flood of hits on the imap server
		dprint "Warning: calling idle more than 15 times in last 1 minute, sleeping for 1 minute to avoid hammering the server\n";
		sleep(60);
	}
	if ($client->IsConnected()) {
		$idle = $client->idle;
		if (!defined($idle)) {
			dprint "ERROR: IDLE failed: $@\n";
			dprint "Reconnecting in 5 seconds\n";
			sleep(5);
			imap_connect();
			$idle = $client->idle;
		}
	} else {
		dprint "disconnected somehow in re_idle?\n";
		imap_connect();
		$idle = $client->idle;
	}
	$idle_start = $now;
	$idling = 1;
}

sub done_if_idle
{
	if ($idling) {
		#reset idle to 0 for all of the working case, if there was a timeout and successfull reconnect, and if it failed and imap_connect got called.
		$idling = 0;
		dprint 2, "Was idling, sending done:\n";
		my $ret = $client->done($idle);
		if (!$ret) {
			if ($client->LastError =~ /^timeout waiting \d+s for data from server/ && $client->IsConnected) {
				dprint "Connection timed out and reconnected.\n";
			} else {
				dprint "ERROR: DONE command returned error: $@\n";
				dprint "Reconnecting in 5 seconds\n";
				sleep(5);
				imap_connect();
			}
		}
	}
}

sub imap_connect
{
	my $res;
	while (!$res) {
		dprint "Connecting...\n";
		$res = ($client = Mail::IMAPClient->new(
				Server   => $server,
				Port     => $port || undef,
				User     => $username,
				Password => $password,
				Starttls => $usestarttls ? () : undef,
				Ssl      => $usessl ? () : undef,
				Timeout  => $timeout,
				Reconnectretry => 2,
				Debug=>(($debug == 3) ? 1 : 0),
			));
		if (!$res) {
			#TODO: somehow print the actual network error (DNS,
			#or connection refused, etc) rather than just
			#"Unable to connect. IO::Socket::IP configuration
			#failed". $client is undef so can't print
			#$client->LastError
			dprint "$@\n";
			if ($@ =~ /LOGIN failed/ 
			    || $@ =~ /Authentication failed/ 
			    || $@ =~ /password/) {
				if ($last_alert_time) {#fix me, use a better check if login has succeeded once already
					$use_libpurple_status = 0;
					$last_alert_time = 0;
					%msg_buffer = ();
					$msg_buffer{"dummy"} = "IMAP IDLE: Login failed after startup, exiting\n";
					send_alerts(clock_gettime(CLOCK_MONOTONIC));
				}
				exit;
			}
			sleep(5);
			#TODO: if it failed to due DNS resolving to the wrong IP, it doesn't re-resolve each try, it should
		}
	}
	$client->select("INBOX");
	dprint "Connected.\n";
	$idling = 0;
}

sub process_msg
{
	my ($msg, $msg_uid) = @_;
	my %msg = %$msg;
	my $score = 0;
	dprint 2, "\n\n\nNEW MESSAGE:\n";
	foreach my $header (keys %notification_rules) {
		dprint 2, "header = $header\n";
		if (!defined($msg->{$header})) {
			if (defined($msg->{Subject})) {
				dprint 2, "$header not defed for msg @{$msg->{Subject}}\n";
			} else {
				dprint 2, "$header not defed for undefed msg...\n";
			}
			next;
		}
		foreach my $ref (@{$notification_rules{$header}}) {
			my ($regex, $val) = @$ref;
			dprint 2, "regex = $regex, val = $val\n";
			dprint 2, "text to match: @{$msg->{$header}}\n";
			if ("@{$msg->{$header}}" =~ /$regex/i) {
				$score += $val;
				dprint 2, "running score = $score\n";
			}
			dprint 2, "score = $score\n";
		}
	}
	if ($score > $min_alert_score) {
		#this gets passed to eval or /bin/sh, so don't be vulnerable to injection
		my $from = sanitize(decode('MIME-Header', "@{$msg->{From}}"));
		my $subject = sanitize(decode('MIME-Header', "@{$msg->{Subject}}"));
		my $msg = " From: $from\n	Subject: $subject, score = $score\n";
		$msg_buffer{$msg_uid} = "$msg";
		dprint "Adding message to notifications: $msg";
		dprint 2, "uid = $msg_uid, number of pending notifications = " . scalar(keys %msg_buffer) . "\n";
		my $now = clock_gettime(CLOCK_MONOTONIC);
		dprint 2, "now = $now, last_alert_time = $last_alert_time, min_period = $min_period\n";
		if ($score > $max_score) {
			$max_score = $score;
		}
	} else {
		dprint "non important message, score = $score, Subject: @{$msg->{Subject}}\n";
	}
}

sub send_alerts {
	my ($now) = @_;
	my $out = "";
	local $/ = "\n";
	#if mails start coming in too fast, batch them
	if (($now - $last_alert_time) < $min_period) {
		if ($max_score < $delayed_alert_score) {
			dprint 2, "" . ($now - $last_alert_time) . " < $min_period, not alerting\n";
			return;
		} else {
			dprint 2, "" . ($now - $last_alert_time) . " < $min_period, but max score of any email = $max_score, proceeding\n";
		}
	} else {
		dprint 2, "" . ($now - $last_alert_time) . " > $min_period, proceeding\n";
	}

	if ($use_libpurple_status) {
		my $status = untaint (`purple-remote getstatus`);
		if (!defined ($status)) {
			$status = "undefined";
		}
		chomp($status);
		if ($status ne "available" &&
		    $status ne "unavailable" &&
		    $status ne "away" &&
		    $status ne "offline" &&
		    1) {
			$out = "error: purple status = $status";
			$a = eval {
				#why does this crash the whole program if it fails?
				my $notify = Desktop::Notify->new();
				my $notification = $notify->create(body => $out, timeout => 0);
				$notification->show();
			};
			if ($@) {
				dprint "ERROR: purple-remote failure notification failed: $@\n";
				my $name = untaint($0);
				system("xmessage \"$name: $out\" &");
				sleep(0.1);
				system("xmessage \"$name: Desktop::Notify failed: $@\" &");
			}
		}
		if ($status eq "unavailable" || $status eq "offline" || $status eq "away") {
			dprint 2, "purple status is $status, not alerting\n";
			dprint 2, "number of pending notifications = " . scalar(keys %msg_buffer) . "\n";
			return;
		}
	}

	if ($mua) {
		my $active_win = sanitize(`xprop -id \$(xprop -root _NET_ACTIVE_WINDOW | cut -d\' \' -f5) WM_NAME`);
		if ($active_win =~ /$mua/) {
			dprint "MUA $mua is active, discarding pending alerts\n";
			%msg_buffer = ();
			return;
		}
	}

	foreach (keys %msg_buffer) {
		$out .= $msg_buffer{$_};
	}
	chomp ($out);
	if ($out ne "") {
		dprint "Alerting now, number of messages = " . scalar(keys %msg_buffer) . "\n";

		$a = eval {
			#why does this crash the whole program if it fails?
			my $notify = Desktop::Notify->new();
			my $notification = $notify->create(body => $out, timeout => 0);
			$notification->show();
		};
		if ($@) {
			dprint "ERROR: notification failed: $@\n";
			my $name = untaint($0);
			system("xmessage \"$name: $out\" &");
			sleep(0.1);
			system("xmessage \"$name: Desktop::Notify failed: $@\" &");
		}
		$last_alert_time = $now;
		$max_score = 0;
	} else {
		dprint "ERROR: send_alerts() called with empty \$out, number of pending notifications = " . scalar(keys %msg_buffer) . "\n";
	}
	%msg_buffer = ();
}

sub cleanup
{
	dprint "cleanup: \n";
	undef $pidfile;
	if (defined($client) && $client->IsAuthenticated()) {
		done_if_idle();
		dprint "logging out\n";
		sleep(0.5);
		$client->logout();
		sleep(0.5);
	} else {
		dprint "\n";
	}
	exit(0);
}

sub get_rules
{
	my ($file) = @_;
	open(F, "< $file") || die("couldn't open file $file: $!\n");
	my $line;
	my %ret;
	while ($line = <F>) {
		if ($line =~ /^(\w+)\s+(.*)$/) {
			my $key = $1;
			my @rules = split(", ", $2);
			dprint 2, "key = $key\n";
			dprint 2, "rules:\n";
			my @rules_struct;
			foreach my $regex (@rules) {
				$regex =~ s/=(-?\d+)$//;
				my $val = $1;
				if (!defined($val) || $val eq "") {
					die "ERROR: regex =>$regex<= doesn't end in ={yourvalue}";
				} else {
					dprint 2, "  regex = $regex, val = $val\n";
					push @rules_struct, [$regex, $val];
				}
			}
			dprint 2, "\n";
			$ret{$key} = \@rules_struct;
		}
	}
	close(F);
	return %ret;
}

sub untaint
{
	my ($a) = @_;
	if (!defined ($a)) {
		return undef;
	}
	if ($a =~ /^([-\@\w\. :\/\x80-\xff]+)$/) {
		$a = $1;
	} elsif ($a eq "") {
		return "";
	} else {
		confess ("Bad data in '$a'");
	}
	return $a;
}

sub sanitize
{
	my ($a) = @_;
	$a =~ s/([^a-zA-Z0-9_ :\n@.\-\x80-\xff])//g; #x80-xff is all non-ascii chars.
	return $a;
}

sub dprint
{
	my $level = $default_debug;
	if ($_[0] =~ /^\d$/) {
		$level = shift;
	}
	if ($debug >= $level) {
		print "" . t_fmt() . "@_";
	}
}

sub t_fmt
{
	return strftime("[%Y-%m-%d %H:%M:%S %z]\t", localtime(time()));
}
